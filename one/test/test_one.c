#include "test_one.h"
#include "one/include/one.h"

#include <assert.h>

static void test_one_a(void)
{
    const unsigned in = 1;
    const int expected = -1;
    const int actual = one(in);
    assert(expected == actual);
}

void test_one(void)
{
    test_one_a();
}
